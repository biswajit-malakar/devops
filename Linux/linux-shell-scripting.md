#### What is shell scripting ?
> Shell scripting means creating a `sh` type file known as `shell file` and we store all kind of *commands, logics* into that file, later we can run that file to execute all the commands.

#### Why do we need shell scripting ?
> Say you are working on a sever and you have 100 linux based system and you need to cnfigure all of them. For configuration you need to run same commands on the terminal for all of them. Wouldn't be so handy if you can somehow create a file that contains all of those commands and you share that file to all system and run that file to execute all the commands ? Here comes the need of shell scripting. You not only just store commands also you can add logices, iterative statement to make life more easier.

#### What is shell ?
> Shell is a program where you can write and execute commands. Shell translate those commands into kernel understandable language for execution.
-  Example:
	- **sh** *(Bourne Shell)* : present in `/bin/sh` directory, sh shell must be present in all linux distros.
	- **Bash** *(Bourne Again Shell)* : present in `/bin/bash` directory, bash may not present in all linux distros.
	- **ZSH** *(Z Bourne Shell)* : present in `/bin/zsh` directory, zsh may not present in all linux distors.
> We can interchange between one shell to another via writting a command called *shebang line*. 
> `#!/bin/sh`
> `#!/bin/bash`
> `#!/bin/zsh`
----

#### Let's start writting bash script...
> Refer [linux user & permision commands](/Linux/linux-user-and-permission.md) and [linux basic commands](/Linux/linux-basic-commands) for more info....
```sh
touch setup.sh                           #Create a setup.sh shell file
vim setup.sh                             #Open setup.sh file on vim editor
	#On vim editor, enter insert mode by pressing `i`
	#!//bin/bash                         #Change default shell to bash shell
	echo "Hello World!"                  #Display "Hello World!" message
	exit                                 #Exit from shell
    #On vim editor, enter command mode by pressing `Esc`, write `:wq` for write and quit
./setup.sh                               #Execute the shell file
#Must get an error message because you don't have permission to execute the file.
sudo chmode u+x setup.sh                 #Give current user permission to execute file
./setup.sh                               #Execute the shell file
"Hello World!"                           #Printing the message
#Now bash shell is exited!
```
---

#### Variables
> In shell scripting we can use variable to store any value and commands as well.
- **Syntax**
> `variable_name=value`                                           # **do not put space!**
> `variable_name=$(command)`                                  # **do not put space!**
- **Example**
`file_name=config.yaml                       # variale storing simple file name`
`create_file=$(touch $file_name)             # variable storing typecased command`
- Example Program
```sh
#!/bin/bash
echo "Hello World!"
file_name=config.yaml
echo "This program is for creating $file_name file"
echo "$file_name is creating now!"
create_file = $(touch $file_name)
echo "$file_name is created now!"
```
---

#### Conditions
> Just like any programming language shell also has conditions.
- **Syntax**
```sh
if [ condition ]                  #Space arround condition is mendetory
then
	commands
elif [ condition ]
then
	commands
else
	commands
fi
```
- **Example Program**
```sh
#!/bin/bash
echo "This program is for showing example of conditions"
folder_name=config
if [ -d $folder_name ]                         #checking $folder_name present or not
	then                                       #if present then
		echo "Showing list of files, folders in $folder_name"
		ls -l #folder_name
	else                                       #if not present
		echo "Folder $folder_name not found!"
		echo "Creating one..."
		mkdir $folder_name
		echo "Folder $folder_name has been created!"
fi                                             #end of if block
```
---

#### Operators 
- **File test operators**

|   **Operator**   | **Task** |
| :---: |--- |
| `-d file` | Checks if file is a directory; if yes, then the condition becomes true. | 
| `-f file` | Checks if file is an ordinary file as opposed to a directory or special file; if yes, then the condition becomes true. |
| `-f file` | Checks if file is readable; if yes, then the condition becomes true. |
| `-w file` | Checks if file is writable; if yes, then the condition becomes true. |
| `-x file` | Checks if file is executable; if yes, then the condition becomes true. |
| `-s file` | Checks if file has size greater than 0; if yes, then condition becomes true. |
| `-e file` | Checks if file exists; is true even if file is a directory but exists. |
| `-g file` | Checks if file has its set group ID (SGID) bit set; if yes, then the condition becomes true |
| `-u file` | Checks if file has its Set User ID (SUID) bit set; if yes, then the condition becomes true. |

- **Arithmetic operator**
```sh
a=10
b=20
```
|   **Operator**   | **Task** | **Exmple** |
| :---: |--- | ---|
| `+` | Adds values on either side of the operator |expr `$a + $b` will give 30 |
| `-` | Subtracts right hand operand from left hand operand | expr `$a - $b` will give -10 |
| `*` | Multiplies values on either side of the operator | expr `$a \* $b` will give 200 |
| `/` | Divides left hand operand by right hand operand | expr `$b / $a` will give 2 |
| `%` | Divides left hand operand by right hand operand and returns remainder | expr `$b % $a` will give 0 |
| `=` | Assigns right operand in left operand | `a = $b` would assign value of b into a |
| `==` | Compares two numbers, if both are same then returns true. | `$a == $b` would return false. |
| `!=` | Compares two numbers, if both are different then returns true. | `$a != $b` would return true. |

- **Relational operator**

|   **Operator**   | **Task** | **Exmple** |
| :---: |--- | ---|
| `-eq` | Checks if the value of two operands are equal or not; if yes, then the condition becomes true. | `$a -eq $b` is not true. |
| `-ne` | Checks if the value of two operands are equal or not; if values are not equal, then the condition becomes true. | `$a -ne $b` is true. |
| `-gt` | Checks if the value of left operand is greater than the value of right operand; if yes, then the condition becomes true. | `$a -gt $b` is not true. |
| `-lt` | Checks if the value of left operand is less than the value of right operand; if yes, then the condition becomes true. | `$a -lt $b` is true. |
| `-ge` | Checks if the value of left operand is greater than or equal to the value of right operand; if yes, then the condition becomes true. | `$a -ge $b` is not true. |
|  `-le` | Checks if the value of left operand is less than or equal to the value of right operand; if yes, then the condition becomes true. | `$a -le $b` is true. |

- **Boolean operator**

|   **Operator**   | **Task** | **Exmple** |
| :---: |--- | ---|
|  `!` | This is logical negation. This inverts a true condition into false and vice versa. | `[ !false ]` is true. |
| `-o` | This is logical **OR**. If one of the operands is true, then the condition becomes true. | `[ $a -lt 20 -o $b -gt 100 ]` is true. |
| `-a` | This is logical **AND**. If both the operands are true, then the condition becomes true otherwise false. | `[ $a -lt 20 -a $b -gt 100 ]` is false. |

- **String operator**

|   **Operator**   | **Task** | **Exmple** |
| :---: |--- | ---|
| `=` | Checks if the value of two operands are equal or not; if yes, then the condition becomes true. | `[ $a = $b ]` is not true. |
| `!=` | Checks if the value of two operands are equal or not; if values are not equal then the condition becomes true. | `[ $a != $b ]` is true. |
| `-z` | Checks if the given string operand size is zero; if it is zero length, then it returns true. | `[ -z $a ]` is not true. |
| `-n` | Checks if the given string operand size is non-zero; if it is nonzero length, then it returns true. | `[ -n $a ]` is not false. |
| `str` | Checks if **str** is not the empty string; if it is empty, then it returns false. | `[ $a ]` is not false. |

---

#### Command line arguments 
> Just like other programming language, shell also support command line arguments. Using command line arguments we can pass values while executing the shell program file. Every value gets an reference name like `$1, $2, $3` and so on. The first argument gets name 1, second one gets name 2 and so on.
- **Example**
```sh
#!/bin/bash
file_name=$1                     # storing first argument as file_name
user_name=$2                     # storing second argument as user_name
group_name=$3                    # stornig third argument as group_name
permissions=$4                   # storing forth argument as permissions

if [ -e $file_name ]
then 
	echo "File name $file_name is already present in this directory!"
else
	echo "Creating file $file_name..."
	touch $file_name
	sleep 1
	echo "Updating user, group info..."
	sudo chown $user_name:$group_name $file_name
	sleep 1
	echo "Updating permission info..."
	sudo chmod $permissions $file_name
fi
sleep 1
echo "exiting..."
# saving file named as command_line_argument.sh
```

>Executing....
>`./command_line_argument.sh temp_file.yaml biswajitmalakar admingroup 777`


> Here's a special variable named as `$*`. This variable stores all the parameters that user have passed on the command line.
> Here's another variable named as `$#`.  This variable stores the number of parameter user have passed on the command line.
- **Example**
```sh
#!/bin/bash
echo "Display all passed parameters : $*"
echo "Number of passed parameters : $#"
```

---

#### Reading user input
> For taking input from an user we need to use this statement `read -p "any message you want to display" variable_name`
- **Example**
```sh
#!/bin/bash
echo "reading...."
sleep 2
read -p "Enter your name : " user_name
read -p "Enter your age : " user_age
echo "writting...."
sleep 2
echo "Hello, your name is $user_name, you are $user_age year(s) old."
```

---

#### Iteration
> Execute some statement line again again for a specific number of times or infinite times.
> Shell has....
- While loop
- For loop
- until loop
- select loop

#### For loop
- **Syntax**
```sh
for var in list
do
	iteration statements
done
```
- **Example**
```sh
#!/bin/bash
# display all the given parameters using command line
for parameter in $*
do
	echo "$parameter"
	sleep 1
done
```

#### While loop 
- **Syntax**
```sh
while [ condition ]
do
	iterative statements
done
```
- **Example1**
```sh
#!/bin/bash
i=0
while [ $i -le 10 ]
do
	echo "$i"
	i=$(($i+1))              # Arithmetic operation can be done inside $((operation))
done
```
- **Example2**
```sh
#!/bin/bash
sum=0
while true
do
	read -p "enter your mark : " user_mark
	if [ "$user_mark" == "q" ]    # In shell programming every input bydefault consider as string
	then
		break
	fi
	sum=$(($sum+$user_mark)) # Arithmetic operation can be done inside $((operation))
done
echo "your total mark is : $sum"
```

---

#### Functions
> Function helps you to group a bunch of statements and you can reuse them via calling that function again again.
- **Syntax for function declaration** 
```sh
function function_name() {
	# statement lines
}
```
- **Syntax for function call**
```sh
function_name parameter1 parameter2 parameter3 ...
```
- **Example**
```sh
#!/bin/bash
function create_file() {
	file_name=$1                     # get first passed parameter 
	echo "creating file $file_name..." 
	touch $file_name
}

create_file hello.txt                # calling function with one parameter
create_file config.yaml              # calling function with one parameter
```
- **return value from function**
```sh
#!/bin/bash
function sum() {
	return $(($1+$2))
}

sum 10 20
result=$?              # get the value returned from last command
# or
result=sum 30 40       # get the returned value from sum function and store into result
echo "the sum is : $result"
```