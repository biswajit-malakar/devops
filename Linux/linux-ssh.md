#### What is SSH ?
> SSH means Secure Shell, ssh helps you to remotely connect to a computer and run commands over that computer via internet connection. To connect to a computer you need to authenticate yourself and you can do this via username and password or via ssh key. Once you are authenticated you can run shell commands on that computer from your local computer and can do lot of stuff.

---

#### SSH commands
| Command | Description |
| :--: |-- |
| `ssh <username>@<ip-address>` | Using this command you can remotely connect to an computer is at given ip address as given user. |
| `ssh-keygen -t rsa -C "comment"` | Generate a key using rsa hashing encryption. |
| `scp <file_name> <username>@<ip-address> <remote_target_folder>` | Copy <file_name> from local system to <remote_target_folder> in remote system. Private key is detected automatically from `.ssh/id_rsa` folder.|
| `scp -i .ssh/id_rsa <file_name> <username>@<ip-address>` | Explicitly providing private key to authenticate. |

> After generating a new key you can find that key in `.ssh/` directory in `/home/username` directory. In `.ssh/` directory you can see private key(`id_rsa`) and public key(`id_rsa.pub`). 
> This key generation happed in your local system. After generating keys in your local system you need to copy those keys to the remote system.
> For that you need to create a `.ssh/` directory (if you do not have one). In `.ssh/` folder there should be `authorized_keys` file initially it's empty. `authorized_keys` contains a list of public keys. Now open `/.ssh/authorized_keys` file with *vim* editor and copy the public key from `.ssh/id_rsa.pub` file in your local system to `.ssh/authorized_keys` file.
> Remember the private key(`.ssh/id_rsa`) is for your local computer DO NOT SHARE to anybody. The public key(`.ssh/id_rsa.pub`) you need to share to your remote system where you want to login like the showed above. The remote system can find your private key using your public key and log you in automatically do not need to enter any password or private key.
> While running the command `ssh <username>@<ip-address>` it automatically search for private key in `.ssh/id_rsa` file. If your private key is anywhere else then you need to  provide that file location explicitly like `ssh -i ./ssh/id_rsa <username>@<ip-address>`.

> In the remote system we can run all commands same as local system from local system.