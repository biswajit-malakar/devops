#### Users
>Linux users can be catagorized into 3 groups...
 1. ==**Root User**== 
 > These users get more privilage than other users. They can do administrative tasks, access ristricted files, add or remove other users etc.
1.  ==**Regular User**==
> These users are regular users who can login to the system and can do regular tasks.
3. ==**Service User **==
>In linux server distros, service users are dedicated to do different different tasks. Like in servers one service user can be dedicated for apache server, another one user can be dedicated for mysql server etc.

---

#### Permissions
> In Linux we can give permission to a user using two ways :
1. ==**User level**==
> In this way we give specific permission for a specific uses. Good for small number of users.
3. ==**Group level**==
> In this way we make a group of users and then give that group some permissions. All user in that group will get same permissions. Good for large number of users.

----

#### Users in practice

|**Command**|**Description**|
| :--: |--- |
|`cat /etc/passwd`|Lets you see all the users present on your system. Format :username:password:uid(user id):gid(group id):gecos(user info):homedir(home directory):shell(shell directory)|
|`sudo adduser <user_name>`|Add a new user. While adding system will also create a new group of same <user_name> and add that user to that separate group.|
|`sudo passwd <user_name>`|Add a new passward or update password for existing <user_name>.|
|`su - <user_name>`|Login your system as other user.|
|`su - `|Login as a root user.|
|`sudo addgroup <group_name>`|Create / add a new group.|
|`cat /etc/group`|Lets you see all the avaliable groups in your system. Format :name:password:gid|

> `adduser`, `addgroup`, `deluser`, `delgroup` commands are user-friendly meaning it will set some automatic value to user info, so you do not need to type that manually. On the other hand `groupadd`, `useradd`, `userdel`, `groupdel` commands requires all info typed manually.

|**Command**|**Description**|
| :--: |--- |
|`sudo usermod -g <group_name> <user_name>`|Modify user, lets you make an user be part of a group.|
|`sudo delgroup <group_name>`|Delete group.|
|`sudo usermod -G <group_name>,<group_name>,..,. <user_name>`|Modify user, lets you make an user be part of multiple groups.|
|`groups`|Lets you see all the groups that the user who logged in belongs to.|
|`groups <user_nmae>`|Lets you see all the groups that the user <user_name> belongs to.|
|`sudo useradd -G <group_name> <user_name>`|Add user, creates a new user and make that user be part of <group_name> and <user_name> groups.|
|`sudo gpasswd -d <user_name> <group_name>`|Delete an user <user_name> from group <group_name>.|
|`ls -l`|Lets you see files and directories in the present directory associated with user ann permissions. Fromat :`<drwxrwxr-x / -rw-rw-r--> <number> <owner user> <owner group> <date> <filename / foldername>`|
|`sudo chown <user_name>:<group_name> <file_name>`|Change owner, change user owner, group owner of a file or folder.|
|`sudo chown <user_nmae> <file_name>`|Change owner, change owner user of a file or folder.|
|`sudo chgrp <group_name> <file_name>`|Change group, change owner group of a file or folder.|

##### Symbols and their meaning
|**Symbol**|**Description**|
| :--: |--- |
|d|Directory|
|-|File|
|c|Character device file|
|l|Symbolic link|

- `drwxrwxr-x`

|**Symbol**|**Description**|
| :--: |--- |
|`d`|*Directory*|
| `rwx`|*Owner user* permissions(`read, write, execute`)|
| `rwx`|*Owner group* permissions(`read, write, execute`)|
|`r-x`|*Other users* except Owner user and Owner group(`read, no permission, execute`)|

- `-rw-rw-r--`

|**Symbol**|**Description**|
| :--: |--- |
| `-`|File|
|`rw-`|Owner user permission(`read, write, no permission`)|
|`rw-`|Owner group permission(`read, write, no permission`)|
|`r-x`|Other users except Owner user and Owner group(`read, no permission, no permission`)|

|**Command**|**Description**|
| :--: |--- |
|`sudo chmod -x <file / foldername>`|Change modify, take away execute permission for all the users and groups.|
|`sudo chmod g-w <file / foldername>`|Change modify, take away write permission from all the groups|
|`sudo chmod g+x <file / foldername>`|Give back / add execute permission for all the groups.|
|`sudo chmod g=rwx <file / foldername>`|Give back / add read, write, execute permissions for all groups.|
|`sudo chmod g=-wx <file / foldername>`|Give back / add no permission, write, execute permissions for all groups.|
|`sudo chmod 777 <file / foldername>`|Add read+write+execute permissions for all users, groups, others.|

|**Symbol**|**Description**|
| :--: |--- |
|`u`|User|
|`g`|Groups|
|`o`|Others|
|`0`|---|
|`1`|--x|
|`2`|-w-|
|`3`|-wx|
|`4`|r--| 
|`5`|r-x|
|`6`|rw-|
|`7`|rwx|