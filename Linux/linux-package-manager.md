####  Brief Intro

>Every software is a bundle of dirrent types of files, and Linux uses dirrent directories for different types of files. So installing a software means you have to arrange all the required files for that software in all different Linux directories unlike Windows. Arranging them menually is so hard and time consumming. A package manager does all those jobs for you. So installing, updating, removing softwares using package manages is very easy. Apt, apt-get, snap are some example of popular package managers.


#### APT v/s APT-GET v/s Snap

>APT is more user friendly then APT-GET. Some softwares are may not avaliable (VS Code, Intelij etc) in APT or APT-GET then you can use Snap package manager. However Snap and APT they both work differently. Snap install all the files of a software as a package in your system on the other hand APT separate all the files based on their types and store them in different directories [Remommanded]. Snap packages are universal suppoorted means snap package (.snap) can be installed in all the linux distros, APT however can install software for debian based (.deb) linux distros. Snap support automatic updates, APT support manual updates. Snap package are larger in size, APT package are smaller in size.

#### Other way to install a software

>A new software may no be present in the official repository so to download the software you can add that package to APT repository via writting a command should provide in the package website this will add that package to /etc/apt/sources.list file, then you can use normal apt command to install that package. Developer create those repository via using PPA (Personal Package Archive) and host them and the path is sharable so you can download there software using repository path.

