
| **Command** | **Description** |
| :--: |-- |
| `ifconfig` | This command lets you see all info about your internet connection. |
| `netstat` | Check for active connection for port numbers. |
| `ps aux` | See all current running processes and how much resources are using. |
| `nslookup <domain_name>` | Sell IP of a domain name. |
| `ping <domain_name>` | See whether a domain is accessable or not. |
