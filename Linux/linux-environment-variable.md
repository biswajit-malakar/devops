
#### What is environment variables ?
> Environment variables are just like normal variables they can reassign their value but those variables has a scope of your environment. These variables are ment to store user informations like user home directory, default shell, default application programs etc. Name of environment variables are in all upper-case letters.

#### Commands for creating, deleting, updating env variables
| **Command** | **Description** |
| :---: |--- |
|  `printenv` | Print all environment variables. |
| `printenv <variable_name>` | Print a specific environment variable. |
| `$variable_name` | Access the content of a variable. |
| `export environment_variable_name=value` | Creating and updating an environment variable in current session. |
|  `unset environment_variable_name` | Unset or delete an environment variable. |

> If you create an environment variable on shell using *export* command that variable is only created for the current session, so if you close the shell and reopen then you can see the variable is gone! 
> However to create an environment variable permanently we have to use `export` command in `.<shell_name>rc` file. For *bash shell* we need to export variables in `.bashrc` file located in `/home/<username` directory. And then to update the changes in `.bashrc` file we have to reload the file using `source .bashrc` command.

---

#### Pass environment variable from one user to another
> Whatever we did previously happed in `.bashrc` (if using bash shell) file which is user specific, present in `/home/<username>` directory. Now to set those environment variables for all users then there is a file named `environment` present in `/etc` directory. Complete path is `/etc/environment`.  In that file `PATH` variable is present there we can add our variable path.
> Let's see an example
```sh
touch hello                                       # Creating an executable hello file
vim hello                                         # Opening hello file on vim editor
	# inside vim editior
	#!/bin/bash                                   # setting default shell to bash
	echo "Hello World! Welcome $USER"             # $USER is a environment variable present in .bashrc file, contains current username
	# exiting from vim editor

# All Done ? No, because if you want to execute this file you need to provide absolute path like this `/home/username/hello`. We dont wanna do this. Insted we can add the path where `hello` file is present. To do that we need to create a PATH variable in `.bashrc` file.

vim .bashrc                                        # Opening .bashrc on vim
	# In .bashrc file you can see so many files just add PATH variable like bellow
	PATH=$PATH:/home/username                      #$PATH variable is present in `/etc/environment` file. We are appending a new path `/home/username` to existing $PATH variable. Notice we are not modifying $PATH variable from `/etc/environment` file.

source .bashrc                                     # Reloading .bashrc file
hello                                              # Now we can run any executable file from `/home/username` directory anywhere for the current user.
```
