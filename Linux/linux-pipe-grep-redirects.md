#### Pipe
>In Linux there is a concept called *piping* meaning that you can pass one program's output into some another program as input.
The pipe in terminal represented by`|` symbol.
- Example:
> To see system logs we can write command ```cat /var/log/syslog```. This will display all the logs on the terminal.
> The output is not that user-friendly, to make it more user-friendly and read page by page you can use a program called ***less***. You can use pipe to do that like this `cat /var/log/syslog | less`. Here I'm saying that whatever the output from the ***cat*** command just pass that output to ***less*** program using pipe.

|**Symbol**|**Description**|
| :--: |--- |
|` `|to go to the next page.|
|`b`|to go to the page before.|
|`q`|to quit from the less program.|

> We can use pipe multiple times in a command.

----

#### Grep
> ***Grep*** is a program lets you filter out any shell output based on the string that you give using regular expression.
- Example
> To see all the commands that you've entered that has `sudo` associated with it. 
`history | grep "sudo"` here we are piping the result of history program to grep for filteration.
- Another Example
> `ls /usr/bin | grep "java"` here list out all the commands present in /usr/bin directory that has java as a name.

---

#### Redirects 

> In Linux there is a ***similer concept like pipe*** is ***redirect*** denoted by `>` symbol on the terminal. It helps you redirect output from one place to another.
- Example
> `history | grep "sudo" > sudo-commands.txt` here I'm saying that get call commands that has *"sudo"* init from my command history and then redirect those output to a new file called *"sudo-commands.txt"* using redirect symbol.
- Example
> `history | grep "rm" > sudo-commads.txt` now if I do this the new data will get overwritten meaning previous data will be gone. `history | grep "rm >> sudo-commands.txt` now if I do this it will append all the output to the *"sudo-commands.txt"* file. `>>` this symbol is used for appending result to the end of a file.

> We can pass multiple commands together using semicolon `;`.
- Example
> `clear; sleep 2; echo "Hello World!"`
