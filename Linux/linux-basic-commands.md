#### Some good stuff to remember!

> **EVERYTHING** in Linux is **FILE**
>  Hit `Tab` for auto complete!
>   `CTRL+r` gives you a reverse search option where you can search for a particular command via typing a part of it.
>   `CTRL+c` helps you to kill current executing command.
>   `CTRL+SHIFT+v` for pesting.
>   `CTRL+SHIFT+c` for copying.

|Shortcut|Description|
| :--: |---- |
| . | Current directory |
| .. | Previous directory |
| ~ | Home directory |
| / | Root directory |

-----

#### Basic linux commands

| Command | Description |
| :---: |--- |
|`pwd`| Print working directory. |
|`ls <absolute_path>`  |List files and folders.|
|`cd <folder_name / absolute_path>`|Change directory.|
|`mkdir <folder_name>`|Make directory.|
|`touch <file_name>`|Create a file.|
|`rm <file_name>`|Remove a file.|
|`rm -r <folder_name>`|Remove a folder recursively. '-r' is a flag.|
|`clear`|Clear terminal output.|
|`mv <folder_old_name> <foler_new_name>`|Rename a folder.|
|`cp -r <from_folder_name> <to_folder_name>`|Copy all files in a folder recursively to another folder.|
|`cp <file_name> <file_name>`|Copy content of a file to another file.|
|`ls -R <folder_name>`|List all files and folders present at given folder.|
|`history`|Show all the previously typed history.|
|`ls -a`|List all files including hidden files.|
|`cat <file_name>`|Concatinate, display file content.|
|`uname -a`|Get basic info about your system.|
|`lscpu`|Get info about your computer CPU.|
|`lsmem`|Get info about your computer memory.|
|`sudo adduser <user_name>`|Add a new user with super user do privilage, without sudo you can not use this command.|
|`su - <user_name>`|Swith user to given username.|