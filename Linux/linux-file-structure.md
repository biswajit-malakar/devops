
| **Directory** | **Description** |
|  :----:       |------           |
|/|This is the root folder, top folder in the hierarchy.|
|/home|*(~)* This folder is for personal data of linux users except root user.|
|/bin|Binaries, contains some basic linux commands. These commands avaliable for all users in the linux system.|
|/sbin|System binaries, to execute you need super user permission. These are system related commands.|
|/lib|Library, essential shared libraries executable from /bin or /sbin use.|
|/usr|User, contains same binaries, system binaries, libraries like the root folder. When we run commands they are basically run from the /usr/bin or /usr/sbin folders.|
|/user/local|This folder also contains /bin, /sbin, /lib folders. These folder is mainly used by third-party softwares that we as an user download. Example - if you download docker then all docker commands will store in /user/local/bin folder.|
|/opt|This folder contains third-party program, software files that are not split into dirrerent components(code editor, browser etc), where the other hand softwares which are split into dirrent components(java, docker etc) are stored into /usr/local folder.|
|/boot|Contains files required for booting your system.|
|/etc|Contains system configuration files and other files requird by the system.|
|/dev|Devices, contains information about all the devices that are connected with the computer.|
|/var|This folder is for storing diffrent logs of different programs.|
|/temp|Temporary, this folder contains temprary files of different programs.|
|/media|This folder contains all the information about mounted medias (hard drive, usb, cd-rom etc).|
|/mnt|Other mounted medias (image files) are stored here.|





