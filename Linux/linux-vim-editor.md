#### Some good stuff to remember!

>**Vim** previously known as **Vi** has two modes:
1. ==**Command Mode**==
> For writting commands for different tasks like quit, save, close etc.
2. ==**Insert Mode**==
>For writing something in the file.

>Press `i` for *Command Mode* --> *Insert Mode*.
>Press `Esc` for *Insert Mode* --> *Command Mode*.

---
#### Basic Commands

| **Command** | **Description** |
| :--: |--- |
|`vim <file_name>`|In Terminal, Open a existing file or create a new one then open in Vim editor.|
|`:wq`|In *Command Mode*, for writting and quit the file.|
|`:q!`|In *Command Mode*, to quit file discard changes.|
|`dd`|In *Command Mode*, to delete a line where your cursor at.|
|`d<integer_number>`|In *Command Mode*, to delete next <integer_number> lines from where your cursor at.|
|`u`|In *Command Mode*, to undo changes.|
|`A`|In *Command Mode*, jump to end of the same line and open Insert Mode.|
|`0`|In *Command Mode*, jump to start of the same line.|
|`$`|In *Command Mode*, jump to end of the same line.|
|`<integer_number>G`|In *Command Mode*, jump to <integer_number> number line.|
|`/<string_pattern>`|In *Command Mode*, search for <string_pattern> in the file and to go to the next match press `n`, to go to the previous match press `N`.|
|`%s/<old_string>/<new_string>`|In *Command Mode*, to replase all <old_string> with <new_string>.|