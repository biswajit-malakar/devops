
#### Git Commands
| Command | Description |
| :---: |--- |
| `git status` | For checking status of your local working repository. |
| `git add <filename>` | Add `<filename>` file onto *staged area* from *unstaged area*. |
| `git add .` | Add all files that are not in *staged area* add them in *staged area* from *unstaged area*. | 
| `git commit` | All of the files that are in *staged area* add them in *local repository*. |
| `git commit -m "message" ` | `-m` flag is for setting a message rest are same. |
| `git log` | Show history of commits that you have made on that folder associated with a *hash id* in *less* program. |
| `git push` | Push commits from *local history* to *remote repository* (if remote repository is connected!). |
| `git init` | Create a *local git repository* on a folder via creating a `.git` file. |
| `git remote add <name> <url/ssh>` | Link a *remote repository *with *local repository* using provided `<url / ssh>`. Also give *remote repository* a name for future reference. Common `<name>` is `origin` for remote repository that you own. |
| `git push --set-upstream <name> <branch name>` | Push commits to a remote repository refered by `<name>` mostly `origin` in a specific branch as `<branch name>` mostly `master` or your own branch name. |
| `git branch` | Shows you all avaliable branches. |
| `git checkout <branch_name>` | Makes `<branch_name>` as your current working branch. |
| `git pull` | Pull down all updates from your remote repository to your local repository. |
| `git checkout -b <branch_name>` | Create and switch to `<branch_name>` in your local repository. |
| `git push --set-upstream <url_name> <branch_name>` | Push the `<branch_name>` to the `<url_name>` repository along will all changes did on `<branch_name>`. |
| `git branch -d <branch_name>` | Delete `<branch_name` branch locally. You need to delete the branch from other branch (master). |
| `git pull -r` | `-r` for Rebase. Useful for merge conflict, if remote branch has some changes that our local branch does not have then use this command to pull all those changes and then do `git push` to push our local changes to remote repository. |
| `git rebase --continue` | Some conflict may not resove by git, then manually we need to fix those conflicts and then if everyone aggreed with those fixes then we can write this command to save that fix then we can push using push command. |



