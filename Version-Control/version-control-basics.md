
#### What is Version Control ?
> Version control is a program that you can download and install in your local system. It keeps tracks all the activity that you can do with a folder or file. Activities like create, delete, update etc. 
> Whenever you make any changes to your file or folder it take a *snapshot like* to that folder and save them to a folder created by version control on your local system.
> Now because of all those *snapshot like* stored in your system and each of them have an unique id. You can use those ids to go back to previous stage.

> Version control like **Git** installed on your local system, but how can you share your project files to other people ? 
> To share your code you can use some services provided by **github**, **gitlab**, **bitbucket** like provides. On their websites you can store your code and multiple people can colaborate with that code.